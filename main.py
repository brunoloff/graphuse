#!/usr/bin/env python

from __future__ import with_statement

from os.path import realpath
from threading import Lock
from errno import ENOENT

import logging

import errno

import os, stat, xattr

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn
import sys

import DBAccess
import Datatypes
from Datatypes import NID_UNDEFINED

import MyErrors

from MyLogger import logger, MyLoggingMixIn
import PathQueries


class Graphuse(MyLoggingMixIn, Operations):
	def __init__(self, dbpath):
		#self.root = realpath(root)
		self.root = ""

		self.rwlock = Lock()

		self.dbFile = dbpath+'/'+"graph.rdb"
		self.fileRoot = dbpath+'/'+"files/"
		self.loop = realpath(self.fileRoot)
		DBAccess.init(self.dbFile)

	def __call__(self, op, path, *args):
		return super(Graphuse, self).__call__(op, self.root + path, *args)

	def access(self, path, mode):
		# # debugFunctionNameArgsValues()


		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(errno.EACCES)
		elif node.type != Datatypes.NT_FILE:
			return
		elif not os.access(self._fileNodePath(node),mode):
			raise FuseOSError(errno.EACCES)


	def chmod(self, path, mode):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(errno.EACCES)
		elif node.type != Datatypes.NT_FILE:
			return
		else:
			os.chmod(self._fileNodePath(node),mode)

	def chown(self, path, uid,gid):
		# debugFunctionNameArgsValues()
		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(errno.EACCES)
		elif node.type != Datatypes.NT_FILE:
			return
		else:
			os.chown(self._fileNodePath(node),uid,gid)

	def create(self, path, mode):
		# debugFunctionNameArgsValues()

		base, sname = os.path.split(path)
		parent = DBAccess.absolutePathNode(base)
		# logger.debug("NODE: " + str(parent))
		child = DBAccess.absolutePathNode(path)

		if parent is None:
			raise FuseOSError(ENOENT) # Parent node not found
		elif child is not None:
			# if file already exists, and we "create" it, then we erase its contents
			return os.open(self._fileNodePath(child), os.O_WRONLY | os.O_CREAT | os.O_TRUNC, mode)
		elif parent.type == Datatypes.NT_FILE:
			node = self._makeVirtualFileSubnode(parent, sname)
			return os.open(self._fileNodePath(node), os.O_WRONLY | os.O_CREAT, mode)
		else:
			node = DBAccess.makeSubNode(parent, sname, Datatypes.NT_FILE)
			return os.open(self._fileNodePath(node), os.O_WRONLY | os.O_CREAT, mode)

	def _fileNodePath(self, node):
		assert isinstance(node, Datatypes.Node)
		assert node.type == Datatypes.NT_FILE

		if "path" not in node.properties:
			raise FuseOSError(ENOENT)
		path = self.fileRoot + node.properties["path"]
		# logger.debug("Node %s, path = %s" % (node, path))
		return path

	def flush(self, path, fh):
		# debugFunctionNameArgsValues()

		return os.fsync(fh)

	def fsync(self, path, datasync, fh):
		# debugFunctionNameArgsValues()
		return os.fsync(fh)

	def getattr(self, path, fh=None):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)


		if node is None:
			raise FuseOSError(ENOENT) # File not found

		# logger.debug("NODE: " + str(node))

		if node.type == Datatypes.NT_FILE: # "real" files are implemented through symbolic links
			st = os.lstat(self._fileNodePath(node))
			ret = dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime', 'st_gid',
			                                               'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))
			return ret
		elif node.type == Datatypes.NT_SEARCH: # search results announce themselves as read-only folders to the OS
			return {'st_mode': stat.S_IFDIR | 0555}
		elif node.type == Datatypes.NT_RELATIONSHIP:
			if node.name[0] == '#': # TODO: Fix Hack!
				return {'st_mode': stat.S_IFLNK | 0775}
			else:
				return {'st_mode': stat.S_IFDIR | 0775}
		else: #if node.type == Node.NT_NODE: # A node appears as a folder by default;
			return {'st_mode': stat.S_IFDIR | 0775, 'st_nlink':2}



			# st = os.lstat(path)
			# logger.debug("ST: " + str(st) + ", " + str(stat.S_IMODE(st.st_mode)))
			# ret = dict((key, getattr(st, key)) for key in (#'st_atime', 'st_ctime',
			#             #'st_gid',
			#             'st_mode', #'st_mtime',
			#              #'st_nlink', #'st_size', 'st_uid'
			#              ))
			# logger.debug("RET: " + str(ret))
			# return ret


	def getxattr(self, path, name, position=0):
		# # debugFunctionNameArgsValues()
		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE:
			xat = xattr.xattr(self._fileNodePath(node))
			try:
				ret = xat.get(name)
			except IOError, e:
				raise FuseOSError(e.errno)
			return ret
		elif node.type == Datatypes.NT_NODE:
			if "xattr."+name not in node.properties:
				raise FuseOSError(93) # TODO FIND standardized place with error codes
			else:
				return node.properties["xattr."+name]
		else:
			raise FuseOSError(93) # TODO FIND standardized place with error codes

	def setxattr(self, path, name, value, options, position=0):
		# debugFunctionNameArgsValues2()
		node = DBAccess.absolutePathNode(path)
		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE:
			xat = xattr.xattr(self._fileNodePath(node))
			try:
				xat.set(name, value) # Options... Ignore for now
			except IOError, e:
				raise FuseOSError(e.errno)
		elif node.type == Datatypes.NT_NODE:
				DBAccess.setProperties(node,{"xattr."+name:value})
		else:
			raise FuseOSError(93) # TODO FIND standardized place with error codes

	def removexattr(self, path, name):
		# debugFunctionNameArgsValues()
		node = DBAccess.absolutePathNode(path)
		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE:
			xat = xattr.xattr(self._fileNodePath(node))
			try:
				xat.remove(name) # Options... Ignore for now
			except IOError, e:
				raise FuseOSError(e.errno)
		elif node.type == Datatypes.NT_NODE:
			DBAccess.removeProperties(node, ["xattr."+name])
		else:
			raise FuseOSError(93) # TODO FIND standardized place with error codes


	def link(self, target, source):
		# debugFunctionNameArgsValues()
		return os.link(source, target)



	def listxattr(self, path):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE: # "real" files are implemented through symbolic links
			xat = xattr.xattr(self._fileNodePath(node))
			try:
				ret = xat.list()
			except IOError, e:
				raise FuseOSError(e.errno)
			return ret
		elif node.type == Datatypes.NT_NODE:
			xatlist = [name[6:] for name in node.properties.keys() if name.startswith(u"xattr.")]
			return xatlist
		else:
			return []



	def mkdir(self, name, mode=0775):
		# debugFunctionNameArgsValues()

		base, sname = os.path.split(name)
		parent = DBAccess.absolutePathNode(base)
		# logger.debug("NODE: " + str(parent))


		if parent is None:
			raise FuseOSError(ENOENT) # Parent node not found
		elif DBAccess.isCommand(sname): # command
			DBAccess.executeCommandOnBaseNode(parent, sname)
		elif parent.type == Datatypes.NT_NODE:
			node = DBAccess.makeSubNode(parent, sname, Datatypes.NT_FILE, Datatypes.RT_NI)
			DBAccess.setProperties(node, {"path":"id"+str(node.id)})
			os.mkdir(self._fileNodePath(node), mode)
		elif parent.type == Datatypes.NT_FILE:
			node = self._makeVirtualFileSubnode(parent, sname)
			os.mkdir(self._fileNodePath(node), mode)

	def _makeVirtualFileSubnode(self, parent, sname):
		return Datatypes.Node(Datatypes.NID_UNDEFINED, Datatypes.NT_FILE, "sname", {"path":parent["path"]+PathQueries.PATH_SEPARATOR+sname})


	def mknod(self, path, mode, device):
		# debugFunctionNameArgsValues()
		pass

	def open(self, path, flags):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)

		logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE:
			return os.open(self._fileNodePath(node), flags)
		else: #if node.type == Node.NT_NODE: # A node appears as a folder by default;
			raise FuseOSError(ENOENT) # TODO Can't open non-file node! One day either allow for it (e.g. to edit properties) or report error correctly


	def read(self, path, size, offset, fh):
		# debugFunctionNameArgsValues()

		os.lseek(fh, offset, 0)
		return os.read(fh, size)


	def readdir(self, path, fh):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)


		if node is None:
			raise FuseOSError(ENOENT) # Node not found

		# logger.debug("NODE: " + str(node))

		if node.type == Datatypes.NT_NODE:
			subnodes = DBAccess.subNodeNames(node)
			relsto = DBAccess.getRelationshipsTo(node)
			relsfrom = DBAccess.getRelationshipsFrom(node)
			relnames = [DBAccess.RELATIONSHIP_PREFIX + rel.GroupNameTo for rel in relsto if rel.id not in [Datatypes.RT_NI, Datatypes.RT_SUPSET]]
			relnames += [DBAccess.RELATIONSHIP_PREFIX + rel.GroupNameFrom for rel in relsfrom]
			return ['.', '..'] + subnodes  + relnames
		elif node.type == Datatypes.NT_FILE:
			return ['.', '..'] + os.listdir(self._fileNodePath(node))
		elif node.type == Datatypes.NT_RELATIONSHIP:
			subnodes = DBAccess.subNodeNames(node)
			return ['.', '..'] + subnodes



		# logger.debug("PATHSPLIT: " + str(self.os_path_split(path)))

		# logger.debug("QUERY: " + self.query_EndOfPathNode(path))

		# query = self.query_EndOfPathNode(path)
		# data, metadata = cypher.execute(graph_db, query)
		# if data[0][0] is not None:   # first row, first column
		#     a = data[0][0]
		#     logger.debug("Node: " + str(a))

		#ref_node = graph_db.get_reference_node()
		# node = getNode(path)
		# return ['.', '..'] + os.listdir(path)

	def readlink(self, path):
		base, name = os.path.split(path)

		return '.'+name
		# path = PathQueries.normalizePath(path)
		# logger.debug("READLINK: path=\""+path+"\"")

		# node = endOfPathNode(path)
		# # logger.debug("NODE: " + str(node))

		# if "type" in node and node["type"] == "file":
		#     if "path" in node:
		#         return node["path"]
		#     else:
		#         raise FuseOSError(ENOENT)
		# else: #if node["type"] == "folder":
		#     raise FuseOSError(ENOENT)

	def release(self, path, fh):
		# debugFunctionNameArgsValues()

		# node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))

		return os.close(fh)

	# Rename is the way basic operations are made.
	# if node is renamed in the same folder, this signals a command is to be executed on that node
	# if node is moved to different node, this typically means it will be linked from that node
	# moving to certain special nodes might have a special behavior, such as deletion (which can be of various kinds)
	def rename(self, old, new):
		# debugFunctionNameArgsValues()

		oldbase, oldname = os.path.split(old)
		newbase, newname = os.path.split(new)

		parent = DBAccess.absolutePathNode(oldbase)
		destination = DBAccess.absolutePathNode(newbase)
		node = DBAccess.absolutePathNode(old)
		newnode = DBAccess.absolutePathNode(new)

		if parent is None or destination is None or node is None:
			raise FuseOSError(ENOENT) # Required nodes are not there!

		if newnode is not None and newnode.type is not Datatypes.NT_FILE:
			raise MyErrors.BadName()

		if node.type is Datatypes.NT_FILE:
			if DBAccess.isCommand(newname):
				DBAccess.executeCommandOnBaseNode(parent, newname)
				# by convention we erase empty folders that run commands
				path = self._fileNodePath(node)
				if os.path.isdir(path) and not os.listdir(path):
					self.rmdir(old)
			elif destination.type in [Datatypes.NT_NODE, Datatypes.NT_RELATIONSHIP]:
				if node.id == NID_UNDEFINED: # subfolder/subfile of a file node
					if newnode is None:
						newnode = DBAccess.makeSubNode(destination, newname, Datatypes.NT_FILE)
					# DBAccess.setProperties(newnode, {"path":"id"+str(newnode.id)})  # REDUNDANT
					os.rename(self._fileNodePath(node),self._fileNodePath(newnode))
				else:
					if newnode is None:
						DBAccess.switchRelationship(parent, node, destination)
						DBAccess.renameNode(node, newname)
					elif newnode.type == Datatypes.NT_FILE:
						DBAccess.removeNode(node)
						os.rename(self._fileNodePath(node),self._fileNodePath(newnode))
					else:
						raise MyErrors.NotPermitted("Can't replace non-file node with file node.")

			elif destination.type is Datatypes.NT_FILE:
				# if destination folder is a file node, then newnode will also be a
				# file node
				# first we move the actual file into its new location
				os.rename(self._fileNodePath(node),self._fileNodePath(newnode))
				if node.id != NID_UNDEFINED:
					# and if the file had a link from a graph node, we remove it
					DBAccess.removeNode(node)


			else: # can not link node to any type other than NT_NODE or NT_RELATIONSHIP
				raise MyErrors.CantLinkNode(newbase)
		elif node.type is Datatypes.NT_NODE:
			if destination.type is Datatypes.NT_NODE:
				if parent.id == destination.id: # Renaming something may execute a command
					if not DBAccess.isCommand(newname):
						return # do nothing TODO eventually some feedback would be nice

					DBAccess.executeCommand(parent, newname, node)
				elif oldname == newname: # If we are just moving it to another node, what we do is create a new edge
					DBAccess.makeRelationship(destination, node)
				else:
					raise FuseOSError(ENOENT) # Can't rename directly on move TODO feedback!
			elif destination.type is Datatypes.NT_RELATIONSHIP:
				DBAccess.makeRelationship(destination, node)
			else: # can not link node to any type other than NT_NODE or NT_RELATIONSHIP
				raise MyErrors.CantLinkNode(newbase)


		# logger.debug("RENAMEOBJS: %s, %s, %s, %s" %(parent, destination, node, newname))



			# return os.rename(old, self.root + new)

	def rmdir(self, path):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)
		# logger.debug("NODE: " + str(parent))


		if node is None:
			raise FuseOSError(ENOENT) # Parent node not found
		elif node.type == Datatypes.NT_NODE:
			pass # Maybe one day I'll allow for soft unlinking, but not now. TODO: proper reporting
		elif node.type == Datatypes.NT_FILE:
			os.rmdir(self._fileNodePath(node)) # will raise error if folder not empty
			if node.id != NID_UNDEFINED:
				# TODO: find out and warn if node has other parents currently all links are severed
				# NOTE:
				DBAccess.removeNode(node)


	def statfs(self, path):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)

		# logger.debug("NODE: " + str(node))
		if node is None:
			raise FuseOSError(ENOENT) # File not found

		if node.type == Datatypes.NT_FILE:
			st = os.statvfs(self._fileNodePath(node))
		else: # filesystem statistics inside the graph are basically those of the folder we store the files in
			st = os.statvfs(self.fileRoot)

		ret = dict((key, getattr(st, key)) for key in ('f_bavail', 'f_bfree', 'f_blocks', 'f_bsize',
		                                               'f_favail', 'f_ffree', 'f_files', 'f_flag', 'f_frsize',
		                                                     'f_namemax'))
		return ret


	def symlink(self, target, source):
		# debugFunctionNameArgsValues()
		pass # return os.symlink(source, target)

	def truncate(self, path, length, fh=None): #TODO
		# debugFunctionNameArgsValues()
		# with open(path, 'r+') as f:
		# 	f.truncate(length)
		pass

	def unlink(self, path):
		# debugFunctionNameArgsValues()

		node = DBAccess.absolutePathNode(path)
		# logger.debug("NODE: " + str(parent))

		if node is None:
			raise FuseOSError(ENOENT)
		elif node.type == Datatypes.NT_NODE:
			pass # Maybe one day I'll allow for soft unlinking, but not now. TODO: proper reporting
		elif node.type == Datatypes.NT_FILE:
			os.unlink(self._fileNodePath(node)) # will raise error if folder not empty
			if node.id != NID_UNDEFINED:
				# TODO: find out and warn if node has other parents currently all links are severed
				# NOTE:
				DBAccess.removeNode(node)


	def utimens(self, path, times=None):
		# debugFunctionNameArgsValues()

		return 0


	def write(self, path, data, offset, fh):
		# # debugFunctionNameArgsValues()
		with self.rwlock:
			os.lseek(fh, offset, 0)
			return os.write(fh, data)


def test_listAllNodes(gf, path="/", prettypath="root"):
	if path == "/":
		logger.critical("ALL NODES:")
	node = DBAccess.absolutePathNode(path)
	logger.critical(prettypath + " : " + str(node))
	if node.type == Datatypes.NT_NODE:
		for subnode in DBAccess.subNodes(node):
			rel = DBAccess._getRelationship(subnode.properties["fromEdgeType"])
			if path == "/":
				test_listAllNodes(gf, path + subnode.name, prettypath + " " + rel.FromNameTo + " " + subnode.name)
			else:
				test_listAllNodes(gf, path + '/' + subnode.name, prettypath + " " + rel.FromNameTo + " " + subnode.name)
	elif node.type == Datatypes.NT_FILE and os.path.isdir(gf._fileNodePath(node)):
		for filename in os.listdir(gf._fileNodePath(node)):
			test_listAllNodes(gf, path + '/' + filename, prettypath + '/' + filename)

def test_methodParams(gf, report, method, *args):
	logger.critical("%s: %s" % (method.upper(), args))
	mth = getattr(gf, method)
	mth(*args)
	if report:
		test_listAllNodes(gf)

import shutil

def cleanslate():
	shutil.rmtree("/Users/brunoloff/Repositories/Programming/Graphuse/.files/")
	os.mkdir("/Users/brunoloff/Repositories/Programming/Graphuse/.files/")
	DBAccess.formatDB()

def runTest(gf):
	logger.setLevel(logging.CRITICAL)
	logger.critical("Logging Level: CRITICAL")
	cleanslate()

	test_listAllNodes(gf)

	test_methodParams(gf, False, "mkdir", "/!mk test")

	test_methodParams(gf, False, "mkdir", "/test/!mks subset")
	test_methodParams(gf, False, "mkdir", "/test/subfolder")
	test_methodParams(gf, True, "mkdir", "/test/subfolder/subsubfolder")

	# test_methodParams(gf, True, "create", "/bla.txt",33188)

	test_methodParams(gf, True, "rename", "/test/subset", "/subset")
	test_methodParams(gf, True, "rename", "/test/subset", "/test/subfolder/subset")

	# test_methodParams(gf, True, "rmdir", "/test/subfolder")
	# test_methodParams(gf, False, "rmdir", "/test/subfolder/subsubfolder")
	# test_methodParams(gf, True, "rmdir", "/test/subfolder")

	logger.setLevel(logging.DEBUG)
	logger.debug("Logging Level: DEBUG")


if __name__ == '__main__':
	argv = sys.argv
	if len(argv) not in (2, 3):
		print('usage: %s <gf file> [optional mountdir]>' % argv[0])
		exit(1)

	gfpath = argv[1]
	if argv[2] is None:
		mntpath = gfpath + "/mntpoint/"
	else:
		mntpath = argv[2]



	# cleanslate()

	gf = Graphuse(gfpath)

	# runTest(gf)
	# logger.setLevel(logging.CRITICAL)
	# logging.basicConfig(level=logging.DEBUG)

	fuse = FUSE(gf, mntpath, foreground=True,nothreads=True)


	# fuse.mkdir("/!mk test")

