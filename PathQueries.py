

import os

from errno import ENOENT
from MyLogger import logger

import stat

PATH_SEPARATOR = '/'

# Thanks John Machin http://stackoverflow.com/questions/4579908/cross-platform-splitting-of-path-in-python
def os_path_split(path):
    parts = []
    while True:
        newpath, tail = os.path.split(path)
        if newpath == path:
            assert not tail
            # if path: parts.append(path)
            break
        if (tail is not ''):
            parts.append(tail)
        path = newpath
    parts.reverse()
    return parts




# this is a hack FIXME, also, do path validation

def normalizePath(path):
	if (path == "/._."):
		return "/"
	return os.path.normpath(path)


def printStMode(st_mode):
    logger.debug("ST_MODE. SOCK LNK  BLK  DIR  CHR  IFO")
    logger.debug("       " + str(bool(st_mode & stat.S_IFSOCK))
        + " " + str(bool(st_mode & stat.S_IFLNK))
        + " " + str(bool(st_mode & stat.S_IFBLK))
        + " " + str(bool(st_mode & stat.S_IFDIR))
        + " " + str(bool(st_mode & stat.S_IFCHR))
        + " " + str(bool(st_mode & stat.S_IFIFO)))
    logger.debug("       " + str(bin(st_mode)))


import shutil

def rmFolderContents(folderpath):
	for root, dirs, files in os.walk(folderpath):
		for f in files:
			os.unlink(os.path.join(root, f))
		for d in dirs:
			shutil.rmtree(os.path.join(root, d))



if __name__ == '__main__':
    logger.debug(os_path_split("/usr/bin/asd"))
    logger.debug(os_path_split("usr/bin/asd"))
    logger.debug(os_path_split("usr/bin/asd/"))
    printStMode(16877)

    logger.debug("bla")