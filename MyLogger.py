import logging

from fuse import *


logger = logging.getLogger(__name__)
hdlr = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)

logger.setLevel(logging.DEBUG)
logger.debug("Logging Level: DEBUG")

import inspect

def debugFunctionNameArgsValues():
	frame  = inspect.stack()[1][0]
	# frame = inspect.currentframe()
	args, _, _, values = inspect.getargvalues(frame)
	ret = inspect.getframeinfo(frame)[2].upper() + ": "
	for i in args[1:]:
		ret += "%s = %s; " % (i, values[i])
	logger.debug(ret)

def debugFunctionNameArgsValues2():
	frame  = inspect.stack()[1][0]
	# frame = inspect.currentframe()
	args, _, _, values = inspect.getargvalues(frame)
	ret = inspect.getframeinfo(frame)[2].upper() + ": "
	for i in args[1:]:
		ret += "%s = %s; " % (i, values[i])
	logger.debug(ret)

def truncStr(string):
	return (string[:75] + ' ...') if len(string) > 255 else string

class MyLoggingMixIn:
	# log = logging.getLogger('MyLog')
	log = logger

	# logops = ['getattr', 'readlink', 'getdir', 'mknod', 'mkdir', 'unlink', 'rmdir', 'symlink', 'rename', 'link', 'chmod', 'chown', 'truncate', 'utime', 'open', 'read', 'write', 'statfs', 'flush', 'release', 'fsync', 'setxattr', 'getxattr', 'listxattr', 'removexattr', 'opendir', 'readdir', 'releasedir', 'fsyncdir', 'init', 'destroy', 'access', 'create', 'ftruncate', 'fgetattr', 'lock', 'utimens', 'bmap']
	logops = ['mknod', 'mkdir', 'unlink', 'rmdir', 'create' ]
	# logops = ['setxattr', 'getxattr', 'listxattr', 'removexattr']
	# logops = []
	def __init__(self):
		# self.logops += ['setxattr', 'getxattr', 'listxattr', 'removexattr']
		# self.logops += ['getattr', 'readdir']
		pass

	def __call__(self, op, path, *args):
		if op in self.logops:
			self.log.debug('-> %s %s %s', op, path, truncStr(repr(args)))
		ret = '[Unhandled Exception]'
		try:
			ret = getattr(self, op)(path, *args)
			return ret
		except OSError, e:
			ret = str(e)
			raise
		finally:
			if op in self.logops:
				self.log.debug('<- %s %s', op, truncStr(repr(ret)))