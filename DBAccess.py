# coding=utf-8

from Datatypes import Node, Relationship, NID_UNDEFINED,NT_UNDEFINED, NT_FILE, NT_NODE, NT_RELATIONSHIP, NT_SEARCH, RT_NI, RT_SUPSET
from MyErrors import NotImplemented
from PathQueries import PATH_SEPARATOR

from errno import ENOENT

import re

from collections import OrderedDict
from PathQueries import os_path_split, normalizePath

from MyLogger import logger

import os

import sqlite3 as mdb

from fuse import FUSE, FuseOSError, Operations, LoggingMixIn

import threading

# dbName = 'graphuse.rdb'

def setDBName(name):
	global dbName
	dbName = name

lcl = threading.local()

def connection():
	if getattr(lcl, "connection", None) is None:
		lcl.connection = mdb.connect(dbName)
	return lcl.connection

# connection() = mdb.connect(, check_same_thread=False)

nodeCache = OrderedDict() # cache with path/node pairs

ROOT_NODE = Node(1, 1, "/")
ORPHANS_NODE = Node(2, 1, ".Orphans")


SPECIFY_RELATIONSHIP_TYPE_IN_PATH = True


def init(dbfile):
	setDBName(dbfile)
	ROOT_NODE.properties = _getNodeProperties(1)
	ORPHANS_NODE.properties = _getNodeProperties(2)


def absolutePathNode(path, getProperties=True):
# 	return relativePathNode(ROOT_NODE, '', os_path_split(normalizePath(path)))
#
# def relativePathNode(path):
	# logger.debug("RELPATHNODE: " + str(startNode) + ", " + startPath + ", " + str(relPathSplit))

	#TODO:
	# if path in nodeCache
	# return nodeCache[path]

	if path in ['/', '']:
		return ROOT_NODE

	parentpath, myname = os.path.split(path)
	parent = absolutePathNode(parentpath, False)

	if parent is None:
		return None

	node = _traverseEdge(parent, myname)

	if node is None:
		return None
	if getProperties and node.id != NID_UNDEFINED:
		node.properties = _getNodeProperties(node.id)
	return node


def formatDB():
	cur = connection().cursor()
	query = """DELETE FROM Nodes;
    DELETE FROM Graph;
    DELETE FROM Relationships;
    DELETE FROM NodeProperties;
    INSERT INTO Nodes(id, type, name) VALUES (1, 1, 'root');
    INSERT INTO Nodes(id, type, name) VALUES (2, 1, '.Orphans');
    INSERT INTO Graph("from", "to", type) VALUES (1, 2, 1);
    INSERT INTO Relationships(id, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom) VALUES (1, '?', '?', 'Elements', 'Parents');
	INSERT INTO Relationships(id, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom) VALUES (2, '?', '?', 'Subsets', 'Supersets');
	"""
	for q in iter(query.splitlines()):
		cur.execute(q)
	connection().commit()


def _traverseEdge(startNode, edgeDescription):
	if startNode.type == NT_NODE:
		return _traverseEdgeNormalNode(startNode, edgeDescription)
	elif startNode.type == NT_FILE:
		node = Node(NID_UNDEFINED, NT_FILE, edgeDescription)
		if "path" not in startNode.properties:
			startNode.properties = _getNodeProperties(startNode.id)
		if "path" not in startNode.properties:
			raise FuseOSError(ENOENT) # TODO: no path property in node file
		else:
			node.properties["path"] = startNode.properties["path"] + PATH_SEPARATOR + edgeDescription
		return node

	# elif startNode.type == NT_UNDEFINED:
	# 	return None # Undefined nodes don't hold anything but themselves
	elif startNode.type == NT_RELATIONSHIP: # TODO traverseEdgeVirtualNode?
		return _traverseEdgeRelationshipNode(startNode, edgeDescription)
	elif startNode.type == NT_SEARCH: # TODO
		pass

def _traverseEdgeRelationshipNode(startNode, edgeDescription):
	assert isinstance(startNode, Node)
	assert isinstance(edgeDescription, basestring)

	cur = connection().cursor()

	if isRelationship(edgeDescription): # Non-Default relationship
		raise NotImplemented("Can't do more than one edge-jump simultaneously.")
	# certain special names refer to undefined nodes, which helps the process of creating new folders in file browsers
	# undefined folders exist for the exclusive purpose of being renamed into something that defines their purpose

	#the node the relationship is about
	assert "relationship" in startNode.properties

	reltype = startNode.properties["relationship"].id
	if "from" in startNode.properties:
		node = startNode["from"]
		dirA = "from"
		dirB = "to"
	else:
		assert "to" in startNode.properties
		node = startNode["to"]
		dirA = "to"
		dirB = "from"


	query = """SELECT
        Nodes.id, Nodes.type
    FROM Nodes, Graph
    WHERE Graph."%s" = ? AND Graph."%s" = Nodes.id AND Graph.type = ? AND Nodes.name = ?
    LIMIT 1""" % (dirA, dirB)
	#logger.debug("QUERY: " + query +"; \n PARAMS: " + str((nodeId, edgeDescription)))
	cur.execute(query, (node.id, reltype,edgeDescription))
	rows = cur.fetchone()
	#logger.debug("OUTCOME: " + str(rows))
	if rows is None or len(rows) == 0:
		return None # NODE NOT FOUND
	nextNode = Node(rows[0], rows[1], edgeDescription)
	if SPECIFY_RELATIONSHIP_TYPE_IN_PATH:
		nextNode.properties["fromEdgeType"] = reltype
	return nextNode



def _traverseEdgeNormalNode(startNode, edgeDescription):
	assert isinstance(startNode, Node)
	assert isinstance(edgeDescription, basestring)

	cur = connection().cursor()

	if isRelationship(edgeDescription): # Non-Default relationship
		rel, direction = _getRelationshipByName(edgeDescToRelName(edgeDescription)) #TODO: proper function to strip # symbol
		if rel is None:
			return None
		return Node(NID_UNDEFINED, NT_RELATIONSHIP,edgeDescription, {"relationship":rel, direction:startNode})
	# certain special names refer to undefined nodes, which helps the process of creating new folders in file browsers
	# undefined folders exist for the exclusive purpose of being renamed into something that defines their purpose
	else:
		nodeId = startNode.id
		query = """SELECT
            Nodes.id, Nodes.type, Graph.type
        FROM Nodes, Graph
        WHERE Graph."from" = ? AND Graph."to" = Nodes.id
                AND Nodes.name = ?
        LIMIT 1"""
		#logger.debug("QUERY: " + query +"; \n PARAMS: " + str((nodeId, edgeDescription)))
		cur.execute(query, (nodeId, edgeDescription))
		rows = cur.fetchone()
		#logger.debug("OUTCOME: " + str(rows))
		if rows is None or len(rows) == 0:
			return None # NODE NOT FOUND
		nextNode = Node(rows[0], rows[1], edgeDescription)
		if SPECIFY_RELATIONSHIP_TYPE_IN_PATH:
			nextNode.properties["fromEdgeType"] = rows[2]
		return nextNode


def _subNodeRows(node):
	cur = connection().cursor()
	query = """SELECT
			Nodes.id, Nodes.type, Nodes.name, Graph.type
		FROM Nodes, Graph
		WHERE Graph."from" = %s AND Graph."to" = Nodes.id
				AND Graph.type IN ( %s, %s )
		""" % (str(node.id), str(RT_NI), str(RT_SUPSET))
	cur.execute(query)
	rows = cur.fetchall()
	if rows is None:
		return []
	return rows

def _nodeRelationshipsTo(nodeid):
	cur = connection().cursor()
	query = """
		SELECT
			Relationships.id, Relationships.FromNameTo, Relationships.ToNameFrom, Relationships.GroupNameTo, Relationships.GroupNameFrom
		FROM Relationships
		WHERE EXISTS(SELECT 1 from Graph where Graph."from" = ?
				AND Graph.type = Relationships.id)
		"""
	cur.execute(query, (nodeid,))
	rows = cur.fetchall()
	if rows is None:
		return []
	return [Relationship(row[0], row[1], row[2], row[3], row[4]) for row in rows]

def _nodeRelationshipsFrom(nodeid):
	cur = connection().cursor()
	query = """SELECT
			Relationships.id, Relationships.FromNameTo, Relationships.ToNameFrom, Relationships.GroupNameTo, Relationships.GroupNameFrom
		FROM Relationships
		WHERE EXISTS(SELECT 1 from Graph where Graph."to" = ?
				AND Graph.type = Relationships.id)
		"""
	cur.execute(query, (nodeid,))
	rows = cur.fetchall()
	if rows is None:
		return []
	return [Relationship(row[0], row[1], row[2], row[3], row[4]) for row in rows]

def _getNode(nodeid):
	cur = connection().cursor()
	query = """SELECT
			type, name
		FROM Nodes
		WHERE id = ?
		"""
	cur.execute(query, (nodeid,))
	row = cur.fetchone()
	if row is None:
		return None
	else:
		return Node(nodeid, row[0], row[1])

def _getRelationship(reltype):
	cur = connection().cursor()
	query = """SELECT
			FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom
		FROM Relationships
		WHERE id = ?
		"""
	cur.execute(query, (reltype,))
	row = cur.fetchone()
	if row is None:
		return None
	else:
		return Relationship(reltype, row[0], row[1], row[2], row[3])

def _getRelationshipByName(name):
	"""
	Returns a pair (rel, direction), where rel is the relationship with given name
	and direction is:
	"to" if name is the contents of one of the columns GroupNameFrom or FromNameTo
	"from" if name is the contents of one of the columns GroupNameTo or ToNameFrom

	Semantics: Suppose we have a relationship  parent -parent-to-> child, so the GroupNameFrom is "parents".
	If we are asking about the GroupNameFrom="Parents" relationship with respect to node N,
	then we want a representative of all nodes P that have P -parent-to-> N, and so N appears in the "to" direction
	 of the relationship. So we should return "to" as direction.
	"""
	cur = connection().cursor()
	query = """SELECT
			id, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom, FromNameTo = ? OR GroupNameTo = ?
		FROM Relationships
		WHERE FromNameTo = ? OR ToNameFrom = ? OR GroupNameTo = ? OR GroupNameFrom = ?
		LIMIT 1
		"""
	cur.execute(query, (name,name,name,name,name,name))
	row = cur.fetchone()
	if row is None:
		return None
	else:
		rel = Relationship(row[0], row[1], row[2], row[3], row[4])
		return [ rel, "to" if row[5]==0 else "from" ]





# def normalNodesUndefinedSubNodeRows(node):
# 	cur = connection.cursor()

# 	query = """SELECT
# 			VirtualNodes.id, VirtualNodes.type, VirtualNodes.name
# 		FROM VirtualNodes, VirtualEdges
# 		WHERE VirtualEdges."from" = %s AND VirtualEdges."to" = VirtualNodes.id
# 				AND VirtualNodes.type = %s
# 		""" % (str(node.id), str(NT_UNDEFINED))
# 	# logger.debug("QUERY: " + query)
# 	cur.execute(query)
# 	rows = cur.fetchall()
# 	if rows is None:
# 		return []
# 	return rows

def subNodes(node):
	if node.type == NT_SEARCH: # Search
		pass
	elif node.type == NT_RELATIONSHIP: # Non-Default relationship
		pass
	else: # if node.type == NT_NODE
		rows = _subNodeRows(node)
		nodes = []
		for row in rows:
			subnode = Node(row[0], row[1], row[2])
			if SPECIFY_RELATIONSHIP_TYPE_IN_PATH:
				subnode.properties["fromEdgeType"] = row[3]
			nodes.append(subnode)
		# rows = normalNodesUndefinedSubNodeRows(node)
		# nodes += [Node(row[0], row[1], row[2]) for row in rows]
		return nodes
	# logger.debug("OUTCOME: " + str(nextNode))


def subNodeNames(node):
		if node.type == NT_SEARCH: # Search
			pass
		elif node.type == NT_RELATIONSHIP: # Non-Default relationship
			reltype = node.properties["relationship"].id
			frm = node.properties.get("from")
			to = node.properties.get("to")
			rows = _SelSubNodesByReltype(reltype, frm, to)
			names = [row[2] for row in rows]
			return names
		else: # if node.type == NT_NODE
			rows = _subNodeRows(node)
			names = [row[2] for row in rows]
			# rows = normalNodesUndefinedSubNodeRows(node)
			# names += [row[2] for row in rows]
			return names


def getRelationshipsTo(node):
	if node.type in  [NT_SEARCH, NT_RELATIONSHIP]: # Search
		return []
	else: # if node.type in [NT_NODE, NT_FILE]
		return _nodeRelationshipsTo(node.id)

def getRelationshipsFrom(node):
	if node.type in  [NT_SEARCH, NT_RELATIONSHIP]: # Search
		return []
	else: # if node.type in [NT_NODE, NT_FILE]
		return _nodeRelationshipsFrom(node.id)



#def isSpecialUndefinedName(objname):
#	return objname.startswith('untitled folder')


COMMAND_PREFIX = '!'
RELATIONSHIP_PREFIX = '#'
VIRTUAL_NODE_PREFIX = '$'


def isCommand(objname):
	return objname[0] == COMMAND_PREFIX


def edgeDescToRelName(edesc):
	if edesc[0] == RELATIONSHIP_PREFIX:
		return edesc[1:]
	elif edesc.startswith('.'+RELATIONSHIP_PREFIX):
		return edesc[2:]

def isRelationship(objname):
	return objname[0] == RELATIONSHIP_PREFIX or objname.startswith('.'+RELATIONSHIP_PREFIX)


def _makeNode(type, name):
	cur = connection().cursor()
	query = """INSERT INTO Nodes (`type`, `name`)
			VALUES
				(?, ?)"""
	#  logger.debug("QUERY: " + query)
	cur.execute(query, (type, name))
	newnode = Node(cur.lastrowid, type, name)
	connection().commit()
	return newnode


def _removeNode(nodeid):
	cur = connection().cursor()
	query = """DELETE FROM Nodes
		WHERE "id" = ?
		"""
	# logger.debug("QUERY: %s, %s" % (query, nodeid))
	cur.execute(query, (nodeid,))
	connection().commit()

def _SelSubNodesByReltype(reltype, frm, to):
	assert frm is not None or to is not None

	cur = connection().cursor()
	nodeid = frm.id if to is None else to.id
	dirA = "from" if to is None else "to"
	dirB = "to" if to is None else "from"
	query = """SELECT
			Nodes.id, Nodes.type, Nodes.name
		FROM Nodes, Graph
		WHERE Graph."%s" = ? AND Graph."%s" = Nodes.id
				AND Graph.type = ?
		""" % (dirA, dirB)
	cur.execute(query, (nodeid, reltype))
	rows = cur.fetchall()
	if rows is None:
		return []
	return rows

def _getNodeProperties(nodeid):
	cur = connection().cursor()
	query = """SELECT
			name, value
		FROM NodeProperties
		WHERE id = ?
		"""
	cur.execute(query, (nodeid,))
	rows = cur.fetchall()
	if rows is None:
		return {}
	else:
		return dict([(row[0], str(row[1])) for row in rows])
		# return dict([(row[0], str(row[1]).encode('hex')) for row in rows])

def _switchEdge(fromid, toid, newfromid, newtoid, newreltype = None):
	cur = connection().cursor()
	if newreltype is None:
		query = """UPDATE Graph
			SET `from` = ?, `to` = ?
			WHERE `from` = ? AND `to` = ?
			"""
		cur.execute(query, (newfromid, newtoid, fromid, toid))
	else:
		query = """UPDATE Graph
			SET `from` = ?, `to` = ?, `type` = ?
			WHERE `from` = ? AND `to` = ?
			"""
		cur.execute(query, (newfromid, newtoid, newreltype, fromid, toid))
	connection().commit()

def _setNodeProperty(nodeid, name, value):
	cur = connection().cursor()
	query = """INSERT OR REPLACE
		INTO NodeProperties (`id`, `name`, `value`)
		VALUES (?, ?, ?)
		"""
	cur.execute(query, (nodeid, name, mdb.Binary(value)))
	connection().commit()

def _removeNodeProperty(nodeid, name):
	cur = connection().cursor()
	query = """DELETE FROM NodeProperties
		WHERE id = ? AND name = ?
		"""
	cur.execute(query, (nodeid, name))
	connection().commit()

def setProperties(node, properties ):
	for key in properties:
		node.properties[key] = properties[key]
		_setNodeProperty(node.id, key, properties[key])

def removeProperties(node, properties ):
	for key in properties:
		node.properties.pop(key, None)
		_removeNodeProperty(node.id, key)


def _makeRelationship(fromnameto, tonamefrom, groupnameto, groupnamefrom):
	cur = connection().cursor()
	query = """INSERT INTO Relationships (`FromNameTo`, `ToNameFrom`, `GroupNameTo`, `GroupNameFrom`)
			VALUES
				(?, ?, ?, ?)"""
	#  logger.debug("QUERY: " + query)
	cur.execute(query, (fromnameto, tonamefrom, groupnameto, groupnamefrom))
	newrel = Relationship(cur.lastrowid, fromnameto, tonamefrom, groupnameto, groupnamefrom)
	connection().commit()
	return newrel


def _makeEdge(frm, to, typ):
	cur = connection().cursor()
	query = """INSERT INTO Graph (`from`, `to`, `type`)
		VALUES
			(?, ?, ?)"""
	# logger.debug("QUERY: " + query)
	cur.execute(query, (frm, to, typ))
	connection().commit()


def _removeEdge(frm, to, typ):
	cur = connection().cursor()
	query = """DELETE FROM Graph
		WHERE "from" = ? AND "to" = ? AND "type" = ?
		"""
	# logger.debug("QUERY: " + query)
	cur.execute(query, (frm, to, typ))
	connection().commit()


def _removeAllEdges(nodeid):
	cur = connection().cursor()
	query = """DELETE FROM Graph
		WHERE "from" = ? OR "to" = ?
		"""
	# logger.debug("QUERY: " + query)
	cur.execute(query, (nodeid, nodeid))
	connection().commit()


def _hasNoEdges(nodeid):
	"""Checks if a node id has no NI or SUPSET edges going into it; a node in "/.Orphans" won't return true"""
	cur = connection().cursor()
	query = """SELECT
			edge_id
		FROM Graph
		WHERE (Graph."from" = ? OR Graph."to" = ?) AND Graph."type" IN (?, ?)
		LIMIT 1"""
	#logger.debug("QUERY: " + query +"; \n PARAMS: " + str((nodeId, edgeDescription)))
	cur.execute(query, (nodeid, nodeid, RT_NI, RT_SUPSET))
	rows = cur.fetchone()

	return rows is None


def _edgeExists(frm, to, typ):
	cur = connection().cursor()
	query = """SELECT
			edge_id
		FROM Graph
		WHERE Graph."from" = ? AND Graph."to" = ? AND Graph."type" = ?
		LIMIT 1"""
	#logger.debug("QUERY: " + query +"; \n PARAMS: " + str((nodeId, edgeDescription)))
	cur.execute(query, (frm, to, typ))
	rows = cur.fetchone()

	return rows is not None


def makeSubNode(parent, name, typ=NT_NODE, reltype=RT_NI, properties={}):
	assert isinstance(parent, Node)
	assert isinstance(name, basestring)
	# can only make children nodes of real, concrete nodes, never virtual nodes
	assert parent.type == NT_NODE
	assert isinstance(typ, int)

	if isRelationship(name):
		assert typ == NT_RELATIONSHIP
		# child = virtualRelationshipNode(parent, name)
		return None #TODO
	else:
		# Add node to db
		child = _makeNode(typ, name)



	# if it is a file node, set its initial path property
	if typ == NT_FILE and "path" not in properties:
		cpath = "id" + str(child.id)
		child.properties["path"] = cpath
		_setNodeProperty(child.id, "path", cpath)

	# Set (remaining) node properties, if any were given
	setProperties(child,properties)


	# INSERT PARENT-CHILD EDGE
	if parent.type == NT_RELATIONSHIP:
		actualParent = parent.properties["from"] if "from" in parent.properties else child
		actualChild = parent.properties["to"] if "to" in parent.properties else child
		actualReltype = parent.properties["relationship"].id
		_makeEdge(actualParent.id, actualChild.id, actualReltype)
	else:
		_makeEdge(parent.id, child.id, reltype)

	return child

# def makeFileSubNode(parent, name):
# 	assert isinstance(parent, Node)
# 	assert isinstance(name, basestring)
# 	# can only make children nodes of real, concrete nodes, never virtual nodes
# 	assert parent.type == NT_NODE

# 	filenode = makeSubNode(parent, name, NT_FILE)

# 	# filepath = "file_" + str(filenode.id)

# 	# filenode["path"] = filepath

# 	# _setNodeProperty(filenode.id, "path", filepath)

# 	return filenode


def virtualRelationshipNode(parent, name):
	# cur.execute(query, (NT_RELATIONSHIP, name))
	# relNode = Node(cur.lastrowid, NT_RELATIONSHIP, name)
	# relNode.properties["from"] = parent
	pass


def renameNode(node, newname):
	_renameNode(node.id, newname)

def _renameNode(nodeid, newname):
	cur = connection().cursor()
	query = """UPDATE Nodes
	SET name=?
	WHERE id=?
	"""
	# logger.debug("Query: %s, %s, %s" % (query, newName, obj.id))
	cur.execute(query, (newname, nodeid))
	connection().commit()


CMD_BAD = 0
CMD_RENAME = 1
CMD_UNLINK = 2
CMD_MAKE_NODE = 3
CMD_MAKE_SUBSET_NODE = 4

COMMAND_SEPARATOR = '[-_\\s\\.]'

CMD_REGEXPS = [
	[CMD_RENAME, re.compile(COMMAND_PREFIX + "(ren|rename)" + COMMAND_SEPARATOR + "(.*)$")],
	[CMD_UNLINK, re.compile(COMMAND_PREFIX + "(ul|unlink)" + "(.*)$")],
	[CMD_MAKE_NODE, re.compile(COMMAND_PREFIX + "(mk|mknode)" + COMMAND_SEPARATOR + "(.*)$")],
	[CMD_MAKE_SUBSET_NODE, re.compile(COMMAND_PREFIX + "(mks|mksubset)" + COMMAND_SEPARATOR + "(.*)$")]
]


def _getCommandParams(code):
	for pair in CMD_REGEXPS:
		m = pair[1].search(code)
		if m is not None:
			return [pair[0], m.groups()]

	return [CMD_BAD, None]


def executeCommand(base, code, obj):
	assert isinstance(base, Node)
	assert isinstance(code, basestring)
	assert isinstance(obj, Node)

	logger.debug("EXECUTE: %s, %s, %s" % (base, code, obj))

	cmdId, params = _getCommandParams(code)

	if cmdId == CMD_BAD:
		pass # TODO Feedback
	elif cmdId == CMD_RENAME:
		_renameNode(obj.id, params[1])
	elif cmdId == CMD_UNLINK:
		unlinkNode(base, obj)
	elif cmdId == CMD_MAKE_NODE:
		makeSubNode(base, params[1], NT_NODE, RT_NI)
		if obj.type == NT_UNDEFINED:
			removeNode(obj)
	elif cmdId == CMD_MAKE_SUBSET_NODE:
		makeSubNode(base, params[1], NT_NODE, RT_SUPSET)
		if obj.type == NT_UNDEFINED:
			removeNode(obj)


def executeCommandOnBaseNode(base, code):
	assert isinstance(base, Node)
	assert isinstance(code, basestring)

	logger.debug("EXECUTE: %s, %s" % (base, code))

	cmdId, params = _getCommandParams(code)

	if cmdId == CMD_BAD:
		pass # TODO Feedback
	elif cmdId == CMD_MAKE_NODE:
		makeSubNode(base, params[1], NT_NODE, RT_NI)
	elif cmdId == CMD_MAKE_SUBSET_NODE:
		makeSubNode(base, params[1], NT_NODE, RT_SUPSET)


def unlinkNode(frm, to, typ=None):
	"""
	Removes relationship between two nodes. Links orphaned nodes to "/.Orphans/", deletes unlinked orphaned nodes

	If frm is a virtual relationship node, then it specifies which relationship to remove (see function "makeRelationship")
	"""
	if frm.id == ORPHANS_NODE.id:
		removeNode(to)
	elif frm.type == NT_RELATIONSHIP:
		relfrom, relto, reltype = _unravelRelNode(frm, to)
		unlinkNode(relfrom, relto, reltype)
	elif frm.type == NT_NODE:
		if typ is None: # By default we unlink NI and SUPSET relations
			_removeEdge(frm.id, to.id, RT_NI)
			_removeEdge(frm.id, to.id, RT_SUPSET)
		else:
			_removeEdge(frm.id, to.id, RT_NI)
		if _hasNoEdges(to.id):
			_makeEdge(ORPHANS_NODE.id, to.id, RT_NI)


def makeRelationship(frm, to, reltype=RT_NI):
	"""
	Make new relationship between two nodes, of the form frm -[reltype]-> to.
	If node is in ORPHANS_NODE, the connection is severed.

	If frm is a virtual relationship node, then it should specify
	its parent in the properties "from" or "to" (depending on the direction of the relationship)
	and the relationship in the property "relationship"
	"""
	assert isinstance(frm, Node)
	assert isinstance(to, Node)
	if frm.type == NT_RELATIONSHIP:
		relfrom, relto, reltype = _unravelRelNode(frm, to)
		makeRelationship(relfrom, relto, reltype)
		removeVirtualNode(frm)
	elif frm.type == NT_NODE:
		_makeEdge(frm.id, to.id, reltype)
		# unlink unorphaned nodes from "/.Orphans"
		if reltype in [RT_NI, RT_SUPSET] and _edgeExists(ORPHANS_NODE.id, to.id, RT_NI):
			_removeEdge(ORPHANS_NODE.id, to.id, RT_NI)


def _unravelRelNode(relnode, othernode):
	assert "from" in relnode.properties or "to" in relnode.properties
	assert "relationship" in relnode.properties
	if "from" in relnode.properties:
		assert isinstance(relnode.properties["from"], Node)
		assert relnode.properties["from"].type == NT_NODE
		return relnode.properties["from"], othernode, relnode.properties["relationship"].id
	elif "to" in relnode.properties:
		assert isinstance(relnode.properties["to"], Node)
		assert relnode.properties["to"].type == NT_NODE
		return othernode, relnode.properties["to"], relnode.properties["relationship"].id


def removeNode(node):
	_removeNode(node.id)
	_removeAllEdges(node.id)


def removeVirtualNode(node):
	if node.id != NID_UNDEFINED:
		removeNode(node)

def switchRelationship(parent, node, newparent):
	if newparent.type == NT_RELATIONSHIP:
		actualParent = newparent.properties["from"] if "from" in newparent.properties else node
		actualChild = newparent.properties["to"] if "to" in newparent.properties else node
		actualReltype = newparent.properties["relationship"].id
		_switchEdge(parent.id, node.id, actualReltype, actualParent.id, actualChild.id)
	else:
		_switchEdge(parent.id, node.id, newparent.id, node.id)

# def subNodes(node):
#     query = PathQueries.subNodesQuery(node)
#     logger.debug("QUERY: query=\""+query+"\"")
#     data, metadata = cypher.execute(graph_db, query)
#     #logger.debug("DATA: data=\""+str(data)+"\"")
#     if len(data) == 0:
#         return []
#     else:
#         return [row[0] for row in data]


# def subNodesQuery(node):
# 	query = "START n = node("+str(node._id)+")\n"
# 	query += "MATCH (n)-[:SUPSET|NI]->(item)\n" # matches everything that is a subset or an element of the node we are looking at
# 	query += "RETURN item"
# 	return query

# def rootNodeQuery():
#     return "START root = node(0) RETURN root"


# def endOfPathNodeQuery(path):
#     parts = os_path_split(path)
#     if parts is None or len(parts) == 0:
#         return rootNodeQuery()
#     query = "START root = node(0)\n"
#     query += "MATCH root"
#     for i in range(len(parts)):
#         query += " -- q" + str(i)
#     query += "\n"

#     query += "WHERE ("
#     for i in range(len(parts)):
#         query += "q"+str(i) + ".name = \""+ parts[i] + "\" and "
#     query = query[:-4] + ")\n"

#     query += "RETURN q" + str(len(parts)-1)
#     return query

def disconnect():
	connection().disconnect()


if __name__ == '__main__':
	root = ROOT_NODE;
	logger.critical("Rootnode %s, %s" % (root, root.properties))
	# foo = makeSubNode(root, "FOO", NT_NODE)
	# foo2 = makeSubNode(root, "FOO2", NT_NODE)
	# bar = makeSubNode(foo, "BAR", NT_NODE)
	# logger.debug("traverse edge root foo: " + str(_traverseEdge(root, "FOO")))
	# logger.debug("SUBNODES ROOT: " + str(subNodes(root)))
	# logger.debug("SUBNODES ROOT: " + str(subNodeNames(root)))
	# logger.debug("SUBNODES FOO: " + str(subNodeNames(foo)))
	# logger.debug("LINK FOO TO FOO2")
	# makeRelationship(foo, foo2, RT_NI)
	# logger.debug("SUBNODES FOO: " + str(subNodeNames(foo)))
	# logger.debug("UNLINK FROM FOO TO FOO2")
	# unlinkNode(foo, foo2, RT_NI)
	# logger.debug("SUBNODES FOO: " + str(subNodeNames(foo)))
	# logger.debug("REMOVE ALL")
	# removeNode(foo)
	# removeNode(foo2)
	# removeNode(bar)
	# logger.debug("SUBNODES ROOT: " + str(subNodeNames(root)))

	# m = CMD_REGEXPS[0][1].search("!ren asd")
	# logger.debug("DB: %s " % (m.group(2),))