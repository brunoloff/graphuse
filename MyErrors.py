from fuse import FuseOSError
import errno

from MyLogger import logger

def NodeNotFound(path):
	communicateError("Node not found at path %s" %(path,))
	return FuseOSError(errno.ENOENT)

def CantLinkNode(path):
	communicateError("Can not link any node from %s" %(path,))
	return FuseOSError(errno.EPERM)

def NotImplemented(mesg):
	communicateError(mesg)
	return FuseOSError(errno.EPERM)

def NotPermitted(mesg):
	communicateError(mesg)
	return FuseOSError(errno.EPERM)

def BadName(mesg = "Name already in use."):
	communicateError(mesg)
	return FuseOSError(errno.EEXIST)

def communicateError(mesg):
	logger.critical(mesg)