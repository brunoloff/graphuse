# a node not in the database has ID:
NID_UNDEFINED = -1
# Database Nodes
NT_NODE = 1
NT_FILE = 2
# Virtual Nodes
NT_UNDEFINED = 0
NT_RELATIONSHIP = 3
NT_SEARCH = 4

RT_ANY = -1

RT_NI = 1
RT_SUPSET = 2


class Node(object):
    """Database node
       """
    __slots__ = ['type', 'id', 'name', 'properties']

    def __init__(self, nodeid, nodetype, name, properties = {}):
        self.id = nodeid
        self.type = nodetype
        self.name = name
        self.properties = dict(properties)

    def __getitem__(self, key):
        if key == 'id':
            return self.id
        elif key == 'type':
            return self.type
        elif key == 'name':
            return self.name
        elif key in self.properties:
        	return self.properties[key]
        else:
            raise IndexError("Invalid subscript "+str(key)+" to Node")

    def __setitem__(self, key, value):
        if key == 'id':
            self.id = value
        elif key == 'type':
            self.type = value
        elif key == 'name':
            self.name = value
        elif isinstance(key, basestring):
        	self.properties[key] = value
        else:
            raise IndexError("Invalid subscript "+str(key)+" to Node")

    def __repr__(self):
    	node_types = {NT_NODE:"nt_node", NT_FILE:"nt_file", NT_RELATIONSHIP:"nt_relationship", NT_SEARCH:"nt_search", NT_UNDEFINED:"nt_undefined"}
    	if self.type in node_types:
        	return u'Node(%s, %s, %s, %s)' % (self.id, node_types[self.type], self.name, self.properties.keys())
        else:
        	return u'Node(%s, nodetype?!, %s)' % (self.id, self.name)

class Relationship(object):
    """Relationship data
       """
    __slots__ = ['id', 'undirected', 'FromNameTo', 'ToNameFrom', 'GroupNameTo', 'GroupNameFrom']

    def __init__(self, relid, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom):
        self.id = relid
        self.FromNameTo = FromNameTo
        self.ToNameFrom = ToNameFrom
        self.GroupNameFrom = GroupNameFrom
        self.GroupNameTo = GroupNameTo
        self.undirected = (FromNameTo == ToNameFrom)


    def __repr__(self):
        return 'Relationship(%s, %s, %s)' % (self.id, self.FromNameTo, self.ToNameFrom)
