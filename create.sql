-- CREATE TABLE Graph (
-- 	edge_id integer NOT NULL,
-- 	"from" integer NOT NULL,
-- 	"to" integer NOT NULL,
-- 	type integer NOT NULL,
-- 	PRIMARY KEY (edge_id)
-- );
-- CREATE TABLE NodeProperties (
--     id integer NOT NULL,
-- 	name text NOT NULL,
-- 	value BLOB,
-- 	PRIMARY KEY (id)
-- );
-- CREATE TABLE Nodes (
-- 	id integer NOT NULL,
-- 	type integer NOT NULL,
-- 	name text NOT NULL,
-- 	PRIMARY KEY (id)
-- );
-- CREATE TABLE Relationships (
-- 	id integer NOT NULL,
-- 	FromNameTo text NOT NULL,
-- 	ToNameFrom text NOT NULL,
-- 	GroupNameTo text NOT NULL,
-- 	GroupNameFrom text NOT NULL,
-- 	PRIMARY KEY (id)
-- );
--
-- CREATE TABLE VirtualEdges (
-- 	edge_id integer NOT NULL,
-- 	"from" integer NOT NULL,
-- 	"to" integer NOT NULL,
-- 	PRIMARY KEY (edge_id)
-- );
-- CREATE TABLE VirtualNodes (
-- 	id integer NOT NULL,
-- 	type integer NOT NULL,
-- 	name text,
-- 	PRIMARY KEY (id)
-- );

DELETE FROM Nodes;
DELETE FROM Graph;
DELETE FROM Relationships;
DELETE FROM NodeProperties;
INSERT INTO Nodes(id, type, name) VALUES (1, 1, 'root');
INSERT INTO Nodes(id, type, name) VALUES (2, 1, '.Orphans');
INSERT INTO Graph("from", "to", type) VALUES (1, 2, 1);
INSERT INTO Relationships(id, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom) VALUES (1, '∋', '∈', 'Subnodes', 'Supernodes');
INSERT INTO Relationships(id, FromNameTo, ToNameFrom, GroupNameTo, GroupNameFrom) VALUES (2, '⊃', '⊂', 'Subsets', 'Supersets');

-- INSERT INTO Nodes(id, type, name) VALUES (2, 1, 'People');
-- INSERT INTO Nodes(id, type, name) VALUES (3, 1, 'Musicians');
-- INSERT INTO Nodes(id, type, name) VALUES (4, 1, 'Friends');
-- INSERT INTO Graph("from", "to", type) VALUES (1, 2, 1);
-- INSERT INTO Graph("from", "to", type) VALUES (2, 3, 2);
-- INSERT INTO Graph("from", "to", type) VALUES (2, 4, 2);


